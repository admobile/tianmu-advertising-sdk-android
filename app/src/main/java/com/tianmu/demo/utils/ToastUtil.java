package com.tianmu.demo.utils;

import android.os.Build;
import android.text.TextUtils;
import android.widget.Toast;

import com.tianmu.TianmuSDK;

/**
 * @author ciba
 * @description 描述
 * @date 2019/1/10
 */
public class ToastUtil {
    public static void toast(String message) {
        if (TianmuSDK.getInstance().getContext() != null && !TextUtils.isEmpty(message)) {
            try {
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N_MR1) {
                    SafeToastUtil.showToast(TianmuSDK.getInstance().getContext(), message, Toast.LENGTH_SHORT);
                } else {
                    Toast.makeText(TianmuSDK.getInstance().getContext(), message, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

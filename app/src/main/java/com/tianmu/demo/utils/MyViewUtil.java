package com.tianmu.demo.utils;

import android.view.View;
import android.view.ViewGroup;

/**
 * @author maipian
 * @description 描述
 * @date 3/18/24
 */
public class MyViewUtil {
    /**
     * 将广告视图添加到容器中的便捷方法
     *
     * @param adContainer 广告容器
     * @param adView      广告视图
     */
    public static void addAdViewToAdContainer(ViewGroup adContainer, View adView) {
        addAdViewToAdContainer(adContainer, adView, null);
    }

    /**
     * 添加广告广告视图到容器中
     *
     * @param adContainer  广告容器
     * @param adView       广告视图
     * @param layoutParams 布局参数
     */
    public static void addAdViewToAdContainer(ViewGroup adContainer, View adView, ViewGroup.LayoutParams layoutParams) {
        if (adContainer == null) {
            return;
        }
        if (adView == null) {
            adContainer.removeAllViews();
            return;
        }
        if (adContainer.getChildCount() > 0 && adContainer.getChildAt(0) == adView) {
            return;
        }
        if (adContainer.getChildCount() > 0) {
            adContainer.removeAllViews();
        }
        if (adView.getParent() != null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }
        if (layoutParams != null) {
            adContainer.addView(adView, layoutParams);
        } else {
            adContainer.addView(adView);
        }
    }
}

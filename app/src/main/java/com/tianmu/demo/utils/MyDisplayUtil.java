package com.tianmu.demo.utils;

import com.tianmu.TianmuSDK;

/**
 * @author maipian
 * @description 描述
 * @date 3/18/24
 */
public class MyDisplayUtil {
    /**
     * dp转像素
     */
    public static int dp2px(int dp) {
        return (int) (TianmuSDK.getInstance().getInitiallyDensity() * dp);
    }
}

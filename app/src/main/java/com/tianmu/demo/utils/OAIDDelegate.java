package com.tianmu.demo.utils;

import android.content.Context;
import android.text.TextUtils;

import com.bun.miitmdid.core.ErrorCode;
import com.bun.miitmdid.core.MdidSdkHelper;
import com.bun.miitmdid.interfaces.IIdentifierListener;
import com.bun.miitmdid.interfaces.IdSupplier;
import com.tianmu.demo.constant.TianmuDemoConstant;

import android.util.Log;

/**
 * oaid获取
 *
 * @author parting_soul
 * @date 2019-10-24
 */
public class OAIDDelegate {

    private OnGetIdCallback mListener;
    private Context mContext;

    public OAIDDelegate(Context context) {
        this.mContext = context.getApplicationContext();
    }

    /**
     * 设置OAID回调
     *
     * @param callback : 回调
     */
    public void setOnGetIdCallback(OnGetIdCallback callback) {
        this.mListener = callback;
    }

    /**
     * 开始获取OAID
     */
    public void startGetOAID() {
        try {
            int result = callFromReflect(mContext);
            String error = "";
            if (result == ErrorCode.INIT_ERROR_DEVICE_NOSUPPORT) {
                error = "不支持的设备";
            } else if (result == ErrorCode.INIT_ERROR_LOAD_CONFIGFILE) {
                error = "加载配置文件出错";
            } else if (result == ErrorCode.INIT_ERROR_MANUFACTURER_NOSUPPORT) {
                error = "不支持的设备厂商";
            } else if (result == ErrorCode.INIT_ERROR_RESULT_DELAY) {
                error = "获取接口是异步的，结果会在回调中返回，回调执行的回调可能在工作线程";
            } else if (result == ErrorCode.INIT_HELPER_CALL_ERROR) {
                error = "反射调用出错";
            }
            if (!TextUtils.isEmpty(error)) {
                Log.e(TianmuDemoConstant.TAG, "Oaid同步获取失败 : " + error);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过反射调用，解决android 9以后的类加载升级，导至找不到so中的方法
     */
    private int callFromReflect(Context cxt) {
        int result = 0;
        try {
            result = MdidSdkHelper.InitSdk(cxt, true, new IIdentifierListener() {
                @Override
                public void OnSupport(boolean b, IdSupplier idSupplier) {
                    if (idSupplier != null && mListener != null) {
                        // 防止异步调用出现异常
                        try {
                            mListener.onIdGetSuccess(idSupplier.getOAID(), idSupplier.getVAID(), idSupplier.getAAID());
                        } catch (Throwable e) {
                        }
                    }
                }
            });
        } catch (Throwable e) {
        }
        return result;
    }

    public interface OnGetIdCallback {
        void onIdGetSuccess(String oaid, String vaid, String aaid);
    }

}

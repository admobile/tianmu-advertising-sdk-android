package com.tianmu.demo.constant;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/26 9:48 AM
 */
public class TianmuDemoConstant {
    public static String TAG = "TianmuDemoLog";

    /**
     * 正式环境
     */
    public static String APP_ID = "1001039";
    /**
     * 正式环境正常开屏
     */
    public static String SPLASH_ID = "f2041be983c6";
    /**
     * 正式环境竞价开屏
     */
    public static String SPLASH_BID_ID = "386d0974ec25";
    /**
     * 横幅广告
     */
    public static String BANNER_ID = "976cdf14258e";
    /**
     * 正常信息流模板
     */
    public static String NATIVE_EXPRESS_ID = "a1bf69c0482d";
    /**
     * 竞价信息流模板
     */
    public static String NATIVE_BID_EXPRESS_ID = "59af67b10423";
    /**
     * 正常信息流自渲染
     */
    public static String NATIVE_ID = "a6b51f34dc89";
    /**
     * 竞价信息流自渲染
     */
    public static String NATIVE_BID_ID = "435fd69c18b7";
    /**
     * 正常插屏
     */
    public static String INTERSTITIAL_ID = "4e950b21f7ca";
    /**
     * 竞价插屏
     */
    public static String INTERSTITIAL_BID_ID = "4071a2ebdf5c";
    /**
     * 正常激励视频
     */
    public static String REWARD_ID = "f1ec72b09d83";
    /**
     * 竞价激励视频
     */
    public static String REWARD_BID_ID = "9d60a754c138";
}

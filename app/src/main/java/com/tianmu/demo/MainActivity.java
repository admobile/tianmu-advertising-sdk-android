package com.tianmu.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.tianmu.TianmuSDK;
import com.tianmu.config.TianmuCustomController;
import com.tianmu.config.TianmuInitConfig;
import com.tianmu.demo.ad.NativeAdActivity;
import com.tianmu.demo.ad.NativeBidAdActivity;
import com.tianmu.demo.ad.NativeBidExpressAdActivity;
import com.tianmu.demo.ad.RewardAdActivity;
import com.tianmu.demo.manager.DeviceInfoManager;
import com.tianmu.demo.utils.OAIDDelegate;
import com.tianmu.demo.utils.SPUtil;
import com.tianmu.demo.widget.PrivacyPolicyDialog;
import com.tianmudemo.BuildConfig;
import com.tianmudemo.R;
import com.tianmu.demo.ad.BannerAdActivity;
import com.tianmu.demo.ad.InterstitialAdActivity;
import com.tianmu.demo.ad.NativeExpressAdActivity;
import com.tianmu.demo.ad.SplashAdActivity;
import com.tianmu.demo.constant.TianmuDemoConstant;

public class MainActivity extends AppCompatActivity {
    private static final String SHOW_PRIVACY_POLICY_DIALOG = "SHOW_PRIVACY_POLICY_DIALOG";
    private static final String AGREE_PRIVACY_POLICY = "AGREE_PRIVACY_POLICY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initListener();

        checkPrivacyPolicy();
    }

    /**
     * 检查隐私政策
     */
    private void checkPrivacyPolicy() {
        // 获取是否已经展示隐私政策
        boolean showPrivacyPolicyDialog = SPUtil.getBoolean(this, SHOW_PRIVACY_POLICY_DIALOG);
        if (showPrivacyPolicyDialog) {
            boolean agreePrivacyPolicy = SPUtil.getBoolean(this, AGREE_PRIVACY_POLICY);
            initAd(agreePrivacyPolicy);
        } else {
            showPrivacyPolicyDialog();
        }
    }

    /**
     * 展示隐私政策弹框
     */
    private void showPrivacyPolicyDialog() {
        PrivacyPolicyDialog privacyPolicyDialog = new PrivacyPolicyDialog(this);
        privacyPolicyDialog.setOnResultCallback(new PrivacyPolicyDialog.OnResultCallback() {
            @Override
            public void onConfirm() {
                SPUtil.putBoolean(getApplicationContext(), SHOW_PRIVACY_POLICY_DIALOG, true);
                // 用户同意之后SP进行记录
                SPUtil.putBoolean(getApplicationContext(), AGREE_PRIVACY_POLICY, true);
                // 初始化广告SDK并加载开屏广告
                initAd(true);
            }

            @Override
            public void onCancel() {
                SPUtil.putBoolean(getApplicationContext(), SHOW_PRIVACY_POLICY_DIALOG, true);
                SPUtil.putBoolean(getApplicationContext(), AGREE_PRIVACY_POLICY, false);
                // 用户不同意初始化天目时，隐私相关都设置为false
                initAd(false);
            }
        });
        privacyPolicyDialog.setCancelable(false);
        privacyPolicyDialog.setCanceledOnTouchOutside(false);
        privacyPolicyDialog.show();
    }

    private void initOaid() {
        OAIDDelegate oaidDelegate = new OAIDDelegate(this);
        oaidDelegate.setOnGetIdCallback(new OAIDDelegate.OnGetIdCallback() {
            @Override
            public void onIdGetSuccess(String oaid, String vaid, String aaid) {
                DeviceInfoManager.getInstance().setOaid(oaid);
                DeviceInfoManager.getInstance().setVaid(vaid);
                DeviceInfoManager.getInstance().setAaid(aaid);
            }
        });
        oaidDelegate.startGetOAID();
    }

    /**
     * 初始化天目广告
     */
    private void initAd(boolean isAgree) {

        if (isAgree) {
            initOaid();
        }

        TianmuSDK.getInstance().init(this, new TianmuInitConfig.Builder()
                .appId(TianmuDemoConstant.APP_ID)
                // 是否开启Debug，开启会有详细的日志信息打印
                // 注意上线后请置为false
                .debug(BuildConfig.DEBUG)
                //【慎改】是否同意隐私政策，将禁用一切设备信息读起严重影响收益
                .agreePrivacyStrategy(isAgree)
                .isCanUseLocation(isAgree)
                .isCanUsePhoneState(isAgree)
                .setTianmuCustomController(new TianmuCustomController() {
                    @Override
                    public String getDevOaid() {
                        return DeviceInfoManager.getInstance().getOaid();
                    }

                    @Override
                    public String getDevVaid() {
                        return DeviceInfoManager.getInstance().getVaid();
                    }
                })
                .build());
    }

    private void initListener() {
        findViewById(R.id.tvSplashAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SplashAdActivity.class));
            }
        });

        findViewById(R.id.tvBanner).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BannerAdActivity.class));
            }
        });

        findViewById(R.id.tvNative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NativeAdActivity.class));
            }
        });

        findViewById(R.id.tvNativeExpress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NativeExpressAdActivity.class));
            }
        });
        findViewById(R.id.tvNativeBidExpress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NativeBidExpressAdActivity.class));
            }
        });

        findViewById(R.id.tvNativeBid).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NativeBidAdActivity.class));
            }
        });

        findViewById(R.id.tvInterstitial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InterstitialAdActivity.class));
            }
        });

        findViewById(R.id.tvRewardVod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RewardAdActivity.class));
            }
        });
    }

}
package com.tianmu.demo.ad;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.tianmu.ad.BannerAd;
import com.tianmu.ad.bean.BannerAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.BannerAdListener;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmudemo.R;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/9 6:02 PM
 */
public class BannerAdActivity extends AppCompatActivity {

    private FrameLayout flContainer;
    private FrameLayout flContainerLoadOnly;

    private BannerAd bannerAd;

    private BannerAd bannerAdLoadOnly;
    private BannerAdInfo bannerAdInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);

        initView();
        initListener();

        loadAd();
    }

    private void initView() {
        flContainer = findViewById(R.id.flContainer);
        flContainerLoadOnly = findViewById(R.id.flContainerLoadOnly);
    }

    private void initListener() {
        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadOnlyAd();
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAd();
            }
        });
    }

    private void loadAd() {
        bannerAd = new BannerAd(this, flContainer);
        bannerAd.setListener(new BannerAdListener() {
            @Override
            public void onAdExpose(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdExpose");
            }

            @Override
            public void onAdClick(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClick");
            }

            @Override
            public void onAdClose(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClose");
            }

            @Override
            public void onAdReceive(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdReceive");
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                Log.d(TianmuDemoConstant.TAG, "onAdFailed error : " + tianmuError.toString());
            }
        });
        bannerAd.loadAd(TianmuDemoConstant.BANNER_ID);
    }

    private void loadOnlyAd () {
        bannerAdLoadOnly = new BannerAd(this);
        bannerAdLoadOnly.setListener(new BannerAdListener() {
            @Override
            public void onAdExpose(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdExpose");
            }

            @Override
            public void onAdClick(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClick");
            }

            @Override
            public void onAdClose(BannerAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClose");
            }

            @Override
            public void onAdReceive(BannerAdInfo adInfo) {
                findViewById(R.id.btnShowAd).setVisibility(View.VISIBLE);
                bannerAdInfo = adInfo;
                Log.d(TianmuDemoConstant.TAG, "onAdReceive");
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                Log.d(TianmuDemoConstant.TAG, "onAdFailed error : " + tianmuError.toString());
            }
        });
        bannerAdLoadOnly.loadAd(TianmuDemoConstant.BANNER_ID);
    }

    private void showAd() {
        findViewById(R.id.btnShowAd).setVisibility(View.GONE);
        flContainerLoadOnly.removeAllViews();
        flContainerLoadOnly.addView(bannerAdInfo.getAdView());
        bannerAdInfo.render();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (bannerAd != null) {
            bannerAd.release();
            bannerAd = null;
        }
        // 释放广告
        if (bannerAdLoadOnly != null) {
            bannerAdLoadOnly.release();
            bannerAdLoadOnly = null;
        }

        if (bannerAdInfo != null) {
            bannerAdInfo.release();
            bannerAdInfo = null;
        }
    }
}

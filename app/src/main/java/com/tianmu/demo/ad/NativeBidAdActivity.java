package com.tianmu.demo.ad;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tianmu.TianmuSDK;
import com.tianmu.ad.NativeAd;
import com.tianmu.ad.bean.NativeAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.NativeAdListener;
import com.tianmu.demo.utils.MyViewUtil;
import com.tianmudemo.R;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.ToastUtil;
import android.util.Log;

import java.util.List;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/11 2:37 PM
 */
public class NativeBidAdActivity extends AppCompatActivity {

    private ConstraintLayout rlAdContainer;
    private FrameLayout flMaterialContainer;
    private TextView tvTitle;
    private TextView tvDesc;
    private TextView tvAdTarget;
    private ImageView ivClose;

    private NativeAd nativeAd;
    private NativeAdInfo nativeAdInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_bid);

        rlAdContainer = findViewById(R.id.rlAdContainer);
        flMaterialContainer = findViewById(R.id.flMaterialContainer);
        tvTitle = findViewById(R.id.tvTitle);
        tvDesc = findViewById(R.id.tvDesc);
        tvAdTarget = findViewById(R.id.tvAdTarget);
        ivClose = findViewById(R.id.ivClose);

        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd();
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

    }

    private void loadAd() {
        ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取中");

        rlAdContainer.setVisibility(View.GONE);
        nativeAd = new NativeAd(this);
        nativeAd.setListener(new NativeAdListener() {

            @Override
            public void onAdReceive(List<NativeAdInfo> adInfos) {
                Log.d(TianmuDemoConstant.TAG, "onAdReceive size:" + adInfos.size());

                for (int i = 0; i < adInfos.size(); i++) {
                    Log.d(TianmuDemoConstant.TAG, "onAdReceive " + adInfos.get(i).toString());
                }

                if (adInfos != null && adInfos.size() > 0) {
                    nativeAdInfo = adInfos.get(0);
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，当前价格：" + nativeAdInfo.getBidPrice() + "(分)");
                }
            }

            @Override
            public void onAdExpose(NativeAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdExpose " + adInfo.toString());
            }

            @Override
            public void onAdClick(NativeAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClick " + adInfo.toString());
            }

            @Override
            public void onAdClose(NativeAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClose " + adInfo.toString());
                if (adInfo != null) {
                    adInfo.release();
                }
                rlAdContainer.setVisibility(View.GONE);
            }

            @Override
            public void onRenderFailed(NativeAdInfo adInfo, TianmuError error) {
                Log.d(TianmuDemoConstant.TAG, "onRenderFailed error : " + error.toString());
                rlAdContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取广告失败");
                Log.d(TianmuDemoConstant.TAG, "onAdFailed error : " + tianmuError.toString());
            }
        });
        nativeAd.loadAd(TianmuDemoConstant.NATIVE_BID_ID);
    }

    private void showAd() {
        if (nativeAdInfo != null) {
            ToastUtil.toast("广告展示");
            rlAdContainer.setVisibility(View.VISIBLE);
            if (nativeAdInfo.isVideo()) {
                View videoView = nativeAdInfo.getMediaView(flMaterialContainer);
                MyViewUtil.addAdViewToAdContainer(flMaterialContainer, videoView);
            } else {
                ImageView imageView = new ImageView(flMaterialContainer.getContext());
                ViewGroup.LayoutParams imageViewLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                TianmuSDK.getInstance().getImageLoader().loadImage(imageView.getContext(), nativeAdInfo.getImageUrl(), imageView);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(imageViewLayoutParams);
                MyViewUtil.addAdViewToAdContainer(flMaterialContainer, imageView);
            }
            if (tvTitle != null) {
                tvTitle.setText(nativeAdInfo.getTitle());
            }
            if (tvDesc != null) {
                tvDesc.setText(nativeAdInfo.getDesc());
            }
            if (tvAdTarget != null) {
                tvAdTarget.setText(nativeAdInfo.getAdTarget());
            }
            nativeAdInfo.registerCloseView(ivClose);
            nativeAdInfo.registerView(rlAdContainer, rlAdContainer);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (nativeAd != null) {
            nativeAd.release();
            nativeAd = null;
        }
    }
}

package com.tianmu.demo.ad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tianmu.ad.InterstitialAd;
import com.tianmu.ad.bean.InterstitialAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.InterstitialAdListener;
import com.tianmu.config.TianmuConfig;
import com.tianmudemo.R;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.ToastUtil;
import android.util.Log;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/27 5:18 PM
 */
public class InterstitialAdActivity extends AppCompatActivity {

    private CheckBox cbLandscape;
    private CheckBox cbBanSensor;
    private CheckBox cbMute;
    private CheckBox cbAutoClose;

    private boolean isLandscape;
    private boolean isBanSensor;
    private boolean isMute;
    private boolean isAutoClose;

    private InterstitialAd interstitialAd;
    private InterstitialAdInfo interstitialAdInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial);

        cbLandscape = findViewById(R.id.cbLandscape);
        cbBanSensor = findViewById(R.id.cbBanSensor);
        cbMute = findViewById(R.id.cbMute);
        cbAutoClose = findViewById(R.id.cbAutoClose);

        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.INTERSTITIAL_BID_ID);
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        findViewById(R.id.btnLoadNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.INTERSTITIAL_ID);
            }
        });

        findViewById(R.id.btnShowNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        cbLandscape.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isLandscape = b;
            }
        });
        cbBanSensor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isBanSensor = b;
            }
        });
        cbMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isMute = b;
            }
        });
        cbAutoClose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isAutoClose = b;
            }
        });
    }

    private void loadAd(final String adPosition) {
        if (adPosition.equals(TianmuDemoConstant.INTERSTITIAL_BID_ID))   {
            ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取中");
        } else {
            ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取中");
        }

        if (interstitialAd != null) {
            interstitialAd.release();
            interstitialAd = null;
        }
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setMute(isMute);
        interstitialAd.setShowDirection(isLandscape ?
                TianmuConfig.ScreenOrientation.LANDSCAPE : TianmuConfig.ScreenOrientation.PORTRAIT);
        interstitialAd.setSensorDisable(isBanSensor);
        if (isAutoClose) {
            interstitialAd.setAutoClose(true, 5);
        }
        interstitialAd.setListener(new InterstitialAdListener() {
            @Override
            public void onAdReceive(InterstitialAdInfo adInfo) {
                ToastUtil.toast("广告获取");
                if (adPosition.equals(TianmuDemoConstant.INTERSTITIAL_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，当前价格：" + adInfo.getBidPrice() + "(分)");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告");
                }
                interstitialAdInfo = adInfo;
                Log.d(TianmuDemoConstant.TAG,"onAdReceive");
            }

            @Override
            public void onAdExpose(InterstitialAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdExpose");
            }

            @Override
            public void onAdClick(InterstitialAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClick");
            }

            @Override
            public void onAdClose(InterstitialAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClose");
            }

            @Override
            public void onVideoStart(InterstitialAdInfo interstitialAdInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoStart");
            }

            @Override
            public void onVideoFinish(InterstitialAdInfo interstitialAdInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoFinish");
            }

            @Override
            public void onVideoPause(InterstitialAdInfo interstitialAdInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoPause");
            }

            @Override
            public void onVideoError(InterstitialAdInfo interstitialAdInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoError");
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                if (adPosition.equals(TianmuDemoConstant.INTERSTITIAL_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取广告失败");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取广告失败");
                }
                Log.d(TianmuDemoConstant.TAG,"onAdFailed error : " + tianmuError.toString());
            }
        });
        interstitialAd.loadAd(adPosition);
    }

    private void showAd() {
        if (interstitialAdInfo != null) {
            ToastUtil.toast("广告展示");
            interstitialAdInfo.showInterstitial(InterstitialAdActivity.this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (interstitialAd != null) {
            interstitialAd.release();
            interstitialAd = null;
        }
        if (interstitialAdInfo != null) {
            interstitialAdInfo.release();
            interstitialAdInfo = null;
        }
    }
}

package com.tianmu.demo.ad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tianmu.ad.SplashAd;
import com.tianmu.ad.bean.SplashAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.SplashAdListener;
import com.tianmudemo.R;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.ToastUtil;
import android.util.Log;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/8 8:06 PM
 */
public class SplashAdActivity extends AppCompatActivity {

    private LinearLayout llSplashContainer;
    private FrameLayout flContainer;
    private CheckBox cbOpenCustomerJumpView;
    private CheckBox cbStatusImmersion;
    private CheckBox cbBanSensor;
    private CheckBox cbMute;
    private TextView tvSkip;

    private SplashAd splashAd;
    private SplashAdInfo splashAdInfo;

    private boolean isOpenCustomerJumpView;
    private boolean isImmersive;
    private boolean isBanSensor;
    private boolean isMute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        llSplashContainer = findViewById(R.id.llSplashContainer);
        flContainer = findViewById(R.id.flContainer);
        cbOpenCustomerJumpView = findViewById(R.id.cbOpenCustomerJumpView);
        cbStatusImmersion = findViewById(R.id.cbStatusImmersion);
        cbBanSensor = findViewById(R.id.cbBanSensor);
        cbMute = findViewById(R.id.cbMute);
        tvSkip = findViewById(R.id.tvSkip);

        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.SPLASH_BID_ID);
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        findViewById(R.id.btnLoadNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.SPLASH_ID);
            }
        });

        findViewById(R.id.btnShowNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        cbStatusImmersion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isImmersive = isChecked;
            }
        });

        cbOpenCustomerJumpView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isOpenCustomerJumpView = isChecked;
            }
        });

        cbBanSensor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBanSensor = isChecked;
            }
        });

        cbMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isMute = isChecked;
            }
        });
    }

    private void loadAd(final String adPosition) {
        if (adPosition.equals(TianmuDemoConstant.SPLASH_BID_ID)) {
            ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取中");
        } else {
            ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取中");
        }

        splashAd = new SplashAd(this);
        splashAd.setImmersive(isImmersive);
        splashAd.setSensorDisable(isBanSensor);
        splashAd.setMute(isMute);
        tvSkip.setVisibility(View.GONE);
        if (isOpenCustomerJumpView) {
            tvSkip.setVisibility(View.VISIBLE);
            splashAd = new SplashAd(this, tvSkip);
            splashAd.setCountDownTime(5000);
        }
        splashAd.setListener(new SplashAdListener() {
            @Override
            public void onAdTick(long millisUntilFinished) {
                Log.d(TianmuDemoConstant.TAG,"onADTick millisUntilFinished:" + millisUntilFinished);
                tvSkip.setText(millisUntilFinished + "/跳过");
            }

            @Override
            public void onAdReceive(SplashAdInfo adInfo) {
                ToastUtil.toast("广告获取");
                if (adPosition.equals(TianmuDemoConstant.SPLASH_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，当前价格：" + adInfo.getBidPrice() + "(分)");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告");
                }
                splashAdInfo = adInfo;
                Log.d(TianmuDemoConstant.TAG,"onAdReceive");
            }

            @Override
            public void onAdExpose(SplashAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdExpose");
            }

            @Override
            public void onAdClick(SplashAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClick");
            }

            @Override
            public void onAdSkip(SplashAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onADSkip");
            }

            @Override
            public void onAdClose(SplashAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClose");
                toMain();
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                if (adPosition.equals(TianmuDemoConstant.SPLASH_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取广告失败");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取广告失败");
                }
                Log.d(TianmuDemoConstant.TAG, "onAdFailed error : " + tianmuError.toString());
                toMain();
            }
        });
        splashAd.loadAd(adPosition);
    }

    private void showAd() {
        if (splashAdInfo != null) {
            ToastUtil.toast("广告展示");
            llSplashContainer.setVisibility(View.VISIBLE);
            flContainer.removeAllViews();
            flContainer.addView(splashAdInfo.getSplashAdView());
            splashAdInfo.render();
        }
    }

    private void toMain() {
        flContainer.removeAllViews();
        llSplashContainer.setVisibility(View.GONE);
        splashAdInfo = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (splashAd != null) {
            splashAd.release();
            splashAd = null;
        }
        if (splashAdInfo != null) {
            splashAdInfo.release();
            splashAdInfo = null;
        }
    }
}

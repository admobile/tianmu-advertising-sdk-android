package com.tianmu.demo.ad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tianmu.ad.RewardAd;
import com.tianmu.ad.bean.RewardAdInfo;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.RewardAdListener;
import com.tianmu.config.TianmuConfig;
import com.tianmudemo.R;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.ToastUtil;
import android.util.Log;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/12/10 11:18 AM
 */
public class RewardAdActivity extends AppCompatActivity {

    private CheckBox cbLandscape;
    private CheckBox cbBanSensor;
    private CheckBox cbMute;

    private boolean isLandscape;
    private boolean isBanSensor;
    private boolean isMute;

    private RewardAd rewardAd;
    private RewardAdInfo rewardAdInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        cbLandscape = findViewById(R.id.cbLandscape);
        cbBanSensor = findViewById(R.id.cbBanSensor);
        cbMute = findViewById(R.id.cbMute);

        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.REWARD_BID_ID);
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        findViewById(R.id.btnLoadNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd(TianmuDemoConstant.REWARD_ID);
            }
        });

        findViewById(R.id.btnShowNormalAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

        cbLandscape.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isLandscape = b;
            }
        });
        cbBanSensor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isBanSensor = b;
            }
        });
        cbMute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isMute = b;
            }
        });
    }

    private void loadAd(final String adPosition) {
        if (adPosition.equals(TianmuDemoConstant.REWARD_BID_ID)) {
            ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取中");
        } else {
            ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取中");
        }

        if (rewardAd != null) {
            rewardAd.release();
            rewardAd = null;
        }
        rewardAd = new RewardAd(this);
        rewardAd.setMute(isMute);
        rewardAd.setShowDirection(isLandscape ?
                TianmuConfig.ScreenOrientation.LANDSCAPE : TianmuConfig.ScreenOrientation.PORTRAIT);
        rewardAd.setSensorDisable(isBanSensor);
        rewardAd.setListener(new RewardAdListener() {

            @Override
            public void onAdReceive(RewardAdInfo adInfo) {
                if (adPosition.equals(TianmuDemoConstant.REWARD_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，当前价格：" + adInfo.getBidPrice() + "(分)");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告");
                }
                ToastUtil.toast("广告获取");
                rewardAdInfo = adInfo;
                Log.d(TianmuDemoConstant.TAG,"onAdReceive");
            }

            @Override
            public void onAdReward(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdReward");
            }

            @Override
            public void onAdExpose(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdExpose");
            }

            @Override
            public void onAdClick(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClick");
            }

            @Override
            public void onAdClose(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onAdClose");
            }

            @Override
            public void onVideoCompleted(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoCompleted");
            }

            @Override
            public void onVideoSkip(RewardAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG,"onVideoSkip");
            }

            @Override
            public void onVideoError(RewardAdInfo adInfo, String msg) {
                Log.d(TianmuDemoConstant.TAG,"onVideoError");
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                if (adPosition.equals(TianmuDemoConstant.REWARD_BID_ID)) {
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取广告失败");
                } else {
                    ((TextView)findViewById(R.id.btnLoadNormalAd)).setText("获取广告，获取广告失败");
                }
                Log.d(TianmuDemoConstant.TAG,"onAdFailed error : " + tianmuError.toString());
            }
        });
        rewardAd.loadAd(adPosition);
    }

    private void showAd() {
        if (rewardAdInfo != null) {
            ToastUtil.toast("广告展示");
            rewardAdInfo.showRewardAd(RewardAdActivity.this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (rewardAd != null) {
            rewardAd.release();
            rewardAd = null;
        }
        if (rewardAdInfo != null) {
            rewardAdInfo.release();
            rewardAdInfo = null;
        }
    }
}

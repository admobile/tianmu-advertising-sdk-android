package com.tianmu.demo.ad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tianmu.ad.NativeExpressAd;
import com.tianmu.ad.bean.NativeExpressAdInfo;
import com.tianmu.ad.entity.AdSize;
import com.tianmu.ad.error.TianmuError;
import com.tianmu.ad.listener.NativeExpressAdListener;
import com.tianmu.demo.utils.MyViewUtil;
import com.tianmudemo.R;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.ToastUtil;
import android.util.Log;

import java.util.List;

/**
 * @Description:
 * @Author: 草莓
 * @CreateDate: 2021/9/11 2:37 PM
 */
public class NativeBidExpressAdActivity extends AppCompatActivity {

    private FrameLayout flContainer;

    private NativeExpressAd nativeExpressAd;
    private NativeExpressAdInfo nativeExpressAdInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native_bid_express);

        flContainer = findViewById(R.id.flContainer);

        findViewById(R.id.btnLoadAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAd();
            }
        });

        findViewById(R.id.btnShowAd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAd();
            }
        });

    }

    private void loadAd() {
        flContainer.setVisibility(View.GONE);
        ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取中");

        int widthPixels = getResources().getDisplayMetrics().widthPixels;
        nativeExpressAd = new NativeExpressAd(this, new AdSize(widthPixels, 0));
        nativeExpressAd.setMute(false);
        nativeExpressAd.setListener(new NativeExpressAdListener() {

            @Override
            public void onAdReceive(List<NativeExpressAdInfo> adInfos) {
                Log.d(TianmuDemoConstant.TAG, "onAdReceive size:" + adInfos.size());

                for (int i = 0; i < adInfos.size(); i++) {
                    Log.d(TianmuDemoConstant.TAG, "onAdReceive " + adInfos.get(i).toString());
                }

                if (adInfos != null && adInfos.size() > 0) {
                    nativeExpressAdInfo = adInfos.get(0);
                    ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，当前价格：" + nativeExpressAdInfo.getBidPrice() + "(分)");
                }
            }

            @Override
            public void onAdExpose(NativeExpressAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdExpose " + adInfo.toString());
            }

            @Override
            public void onAdClick(NativeExpressAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClick " + adInfo.toString());
            }

            @Override
            public void onAdClose(NativeExpressAdInfo adInfo) {
                Log.d(TianmuDemoConstant.TAG, "onAdClose " + adInfo.toString());
                if (adInfo != null) {
                    adInfo.release();
                }
                flContainer.setVisibility(View.GONE);
            }

            @Override
            public void onRenderFailed(NativeExpressAdInfo adInfo, TianmuError error) {
                Log.d(TianmuDemoConstant.TAG, "onRenderFailed error : " + error.toString());
                flContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailed(TianmuError tianmuError) {
                ((TextView)findViewById(R.id.btnLoadAd)).setText("获取广告，获取广告失败");
                Log.d(TianmuDemoConstant.TAG, "onAdFailed error : " + tianmuError.toString());
            }
        });
        nativeExpressAd.loadAd(TianmuDemoConstant.NATIVE_BID_EXPRESS_ID);
    }

    private void showAd() {
        if (nativeExpressAdInfo != null) {
            flContainer.setVisibility(View.VISIBLE);
            ToastUtil.toast("广告展示");
            View nativeView = nativeExpressAdInfo.getNativeExpressAdView();
            MyViewUtil.addAdViewToAdContainer(flContainer, nativeView);
            nativeExpressAdInfo.render();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 释放广告
        if (nativeExpressAd != null) {
            nativeExpressAd.release();
            nativeExpressAd = null;
        }
    }

}

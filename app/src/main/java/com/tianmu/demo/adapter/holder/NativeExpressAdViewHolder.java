package com.tianmu.demo.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tianmu.ad.bean.NativeExpressAdInfo;
import com.tianmu.ad.listener.VideoListener;
import com.tianmu.ad.model.INativeAd;
import com.tianmu.demo.constant.TianmuDemoConstant;
import com.tianmu.demo.utils.MyViewUtil;
import com.tianmudemo.R;

/**
 * 信息流模板广告ViewHolder
 */
public class NativeExpressAdViewHolder extends RecyclerView.ViewHolder {

    public NativeExpressAdViewHolder(@NonNull ViewGroup viewGroup) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_native_ad_express_ad, viewGroup, false));
    }

    public void setData(NativeExpressAdInfo nativeExpressAdInfo) {
        if (nativeExpressAdInfo != null) {
            // 注册视频监听
            setVideoListener(nativeExpressAdInfo);
            // 获取广告视图
            View view = nativeExpressAdInfo.getNativeExpressAdView();
            // 视图添加至布局
            MyViewUtil.addAdViewToAdContainer(((ViewGroup) itemView), view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            // 渲染广告，一定要最后调用
            nativeExpressAdInfo.render();
        }
    }

    public void setVideoListener(NativeExpressAdInfo nativeExpressAdInfo) {
        if (nativeExpressAdInfo != null && nativeExpressAdInfo.isVideo()) {
            nativeExpressAdInfo.setVideoListener(new VideoListener() {
                @Override
                public void onVideoStart(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoStart");
                }

                @Override
                public void onVideoFinish(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoFinish");
                }

                @Override
                public void onVideoPause(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoPause");
                }

                @Override
                public void onVideoError(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoError");
                }
            });
        }
    }
}

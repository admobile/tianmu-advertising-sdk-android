package com.tianmu.demo.adapter.holder;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.tianmu.TianmuSDK;
import com.tianmu.ad.bean.NativeAdInfo;
import com.tianmu.ad.listener.VideoListener;
import com.tianmu.ad.model.INativeAd;
import com.tianmu.demo.constant.TianmuDemoConstant;
import android.util.Log;

import com.tianmu.demo.utils.MyViewUtil;
import com.tianmudemo.R;

/**
 * 信息流自渲染广告ViewHolder
 */
public class NativeAdViewHolder extends RecyclerView.ViewHolder {

    private ConstraintLayout rlAdContainer;
    private FrameLayout flMaterialContainer;
    private TextView tvTitle;
    private TextView tvDesc;
    private TextView tvAdTarget;
    private ImageView ivClose;

    public NativeAdViewHolder(@NonNull ViewGroup viewGroup) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_native_ad, viewGroup, false));
        rlAdContainer = itemView.findViewById(R.id.rlAdContainer);
        flMaterialContainer = itemView.findViewById(R.id.flMaterialContainer);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvDesc = itemView.findViewById(R.id.tvDesc);
        tvAdTarget = itemView.findViewById(R.id.tvAdTarget);
        ivClose = itemView.findViewById(R.id.ivClose);
    }

    public void setData(NativeAdInfo nativeAdInfo) {
        if (nativeAdInfo != null) {
            // 注册视频监听
            setVideoListener(nativeAdInfo);
            // 判断是否为视频类广告
            if (nativeAdInfo.isVideo()) {
                // 获取视频视图控件
                View videoView = nativeAdInfo.getMediaView(flMaterialContainer);
                // 添加进布局
                MyViewUtil.addAdViewToAdContainer(flMaterialContainer, videoView);
            } else {
                ImageView imageView = new ImageView(flMaterialContainer.getContext());
                ViewGroup.LayoutParams imageViewLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                // 获取图片并展示
                TianmuSDK.getInstance().getImageLoader().loadImage(imageView.getContext(), nativeAdInfo.getImageUrl(), imageView);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(imageViewLayoutParams);
                MyViewUtil.addAdViewToAdContainer(flMaterialContainer, imageView);
            }
            if (tvTitle != null) {
                // 设置标题
                tvTitle.setText(nativeAdInfo.getTitle());
            }
            if (tvDesc != null) {
                // 设置正文
                tvDesc.setText(nativeAdInfo.getDesc());
            }
            if (tvAdTarget != null) {
                // 设置渠道
                tvAdTarget.setText("天目广告");
            }
            // 注册关闭事件
            nativeAdInfo.registerCloseView(ivClose);
            // 注册视图和点击事件
            nativeAdInfo.registerView(rlAdContainer, rlAdContainer);
        }
    }

    public void setVideoListener(NativeAdInfo nativeAdInfo) {
        if (nativeAdInfo != null && nativeAdInfo.isVideo()) {
            nativeAdInfo.setVideoListener(new VideoListener() {
                @Override
                public void onVideoStart(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoStart");
                }

                @Override
                public void onVideoFinish(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoFinish");
                }

                @Override
                public void onVideoPause(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoPause");
                }

                @Override
                public void onVideoError(INativeAd nativeAd) {
                    Log.d(TianmuDemoConstant.TAG, "onVideoError");
                }
            });
        }
    }
}

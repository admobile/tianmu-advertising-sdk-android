package com.tianmu.demo.manager;

/**
 * @author 草莓
 * @description 设备信息管理类
 * @date 2023/1/31
 */
public class DeviceInfoManager {
    private volatile static DeviceInfoManager instance;

    private String oaid;
    private String vaid;
    private String aaid;

    public static DeviceInfoManager getInstance() {
        if (instance == null) {
            synchronized (DeviceInfoManager.class) {
                if (instance == null) {
                    instance = new DeviceInfoManager();
                }
            }
        }
        return instance;
    }

    public String getOaid() {
        return oaid;
    }

    public void setOaid(String oaid) {
        this.oaid = oaid;
    }

    public String getVaid() {
        return vaid;
    }

    public void setVaid(String vaid) {
        this.vaid = vaid;
    }

    public String getAaid() {
        return aaid;
    }

    public void setAaid(String aaid) {
        this.aaid = aaid;
    }
}
